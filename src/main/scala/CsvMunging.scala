import scala.io.Source
import java.io.File
import java.io.PrintWriter


object CsvMunging {

  def simpleFilter() {
    val source = Source.fromFile("yahoo_stocks.csv")

    // Load all the line into a list. Not each line is a full string
    val dataAndHeader = source.getLines.toList
    
    // Remove the header
    val data = dataAndHeader.tail
    
    // split each line into Array of strings
    val splitData = data.map(_.split(",").toList)
    
    // filter by year
    val filteredData = splitData.filter(row => row(0).substring(0, 4) == "2010")
    
    // Write the filtered data to a traget csv file
    
    val output = new PrintWriter(new File("filtered_yahoo_stocks.csv"))
    filteredData.foreach(row => {
      output.write( row.mkString(",") + "\n" )
    })
    
    output.close()
  }
  
  def monthlyAvgAdjClose() {
    
    val source = Source.fromFile("yahoo_stocks.csv")

    // Load all the line into a list. Not each line is a full string
    val dataAndHeader = source.getLines.toList
    
    // Remove the header
    val data = dataAndHeader.tail
    
    // Build a new list with only the needed data. year-month and adj close. 
    // adj close should be float
    val closeData = data.map(row => {
      val splitRow = row.split(",")
      val splitDate = splitRow(0).split("-")
      
      List(s"${splitDate(0)}-${splitDate(1)}", splitRow(6))
    })
    
    val groupedData = closeData.groupBy(_.head) 
    
    val result = groupedData.map({
      case (key, value) => 
        val sum = value.foldLeft(0.0f)((sum, row) => sum + row(1).toFloat)
        List(key, sum / value.length)
    })
    
    val output = new PrintWriter(new File("yahoo_stocks_monthly_avg.csv"))
    output.write(
        result.map(_.mkString(",")).mkString("\n")
    )
    
    output.close()
  }
  
  def main(args: Array[String]) {
    monthlyAvgAdjClose()
  }
  
}
