object PascalTriangle {

  // I don't handle any negetive cases 0 or -ve numbers
  def pascal(row: Int): List[List[Int]] = row match {
    case 1           => List(List(1))
    case 2           => pascal(1) :+ List(1, 1)
    case x           =>
      val tree = pascal(row - 1)
      val thisRow = (1 :: tree.last.sliding(2).toList.map(_.sum)) :+ 1
      tree :+ thisRow
  }

  def main(args: Array[String]): Unit = {
    print( pascal(5).map(_.mkString(" ")).mkString("\n") )
  }

}