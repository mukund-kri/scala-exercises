object ParenMatch {

  // Check for balanced parens only
  def isBalanced(inputString: String): Boolean = {
    def calcBalanced(input: List[Char], openParen: Int): Boolean = (input, openParen) match {
      case (List(), openParen) => openParen == 0
      case ('(' :: xs, openParen) => calcBalanced(xs, openParen + 1)
      case (')' :: xs, openParen) if openParen > 0 => calcBalanced(xs, openParen - 1)
      case (')' :: xs, openParen) if openParen <= 0 => false
      case (x :: xs, openParen) => calcBalanced(xs, openParen)
    }
    calcBalanced(inputString.toList, 0)
  }

  // this one checks for open close of 3 block symbols [, {, (
  def isMultiBalanced(inputString: String): Boolean = {

    val openSymbols = Set('(', '{', '[')
    val closeSymbols = Set(')', '}', ']')
    def closeToOpenMap(openSymbol: Char) = openSymbol match {
      case ')'  => '('
      case '}'  => '{'
      case ']'  => '['
      case x    => x
    }

    def calcBalanced(input: List[Char], openTracker: List[Char]): Boolean = (input, openTracker) match {
      case (List(), tracker) => tracker.isEmpty
      case (x :: xs, tracker) if openSymbols.contains(x) => calcBalanced(xs, x :: tracker)
      case (x :: xs, tracker) if closeSymbols.contains(x) =>
        if (tracker.head == closeToOpenMap(x))
          calcBalanced(xs, tracker.tail)
        else
          false
      case (x :: xs, tracker) => calcBalanced(xs, tracker)
    }

    calcBalanced(inputString.toList, List())
  }

  def main(args: Array[String]): Unit = {
    print( isMultiBalanced("[[][([)]]]") )

  }

}