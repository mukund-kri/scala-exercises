object PlusMinus {
  
  def compute(n: Int, sum: Int): Int = n match {
    case 0  => if (sum == 0) 1 else 0
    case x  => compute(n - 1, sum + x) + compute(n - 1, sum - x)
  } 
  
  def main(args: Array[String]) {
     println( compute(7, 0) )  
  }
}